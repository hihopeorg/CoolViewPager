# CoolViewPager

**本项目是基于开源项目 CoolViewPager 进行 ohos 化的移植和开发的，可以通过项目标签以及 github 地址（ https://github.com/HuanHaiLiuXin/CoolViewPager ）追踪到原项目版本**

#### 项目介绍

- 项目名称：CoolViewPager
- 所属系列：ohos的第三方组件适配移植
- 功能：PagerSlider页面切换，滑动方向设置，自动轮播以及轮播间隔时长等。
- 项目移植状态：完成
- 调用差异：无差异。
- 项目作者和维护人：hihope
- 联系方式：hihope@hoperun.com
- 原项目Doc地址：https://github.com/HuanHaiLiuXin/CoolViewPager
- 基线release版本：V1.0.0,SHA1:6f6c4fcb3a271edafdc2ff008f7636c58954af47
- 编程语言：Java
- 外部库依赖：无

#### 效果演示
![gif](preview.gif)

#### 安装教程
方式一:
1. 编译CoolViewPager的har包CoolViewPager.har。
2. 启动 DevEco Studio，将har包导入工程目录“entry->libs”下。
3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。
```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
	……
}
```

在导入的har包上点击右键，选择“Add as Library”对包进行引用，选择需要引用的模块，并点击“OK”即引用成功。

方式二:

1.在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址
```java
    repositories {
        maven {
            url 'http://106.15.92.248:8081/repository/Releases/' 
        }
    }
```

2.在应用模块的build.gradle的dependencies闭包中，添加如下代码:
```
    dependencies {
        implementation 'com.huanhailiuxin.coolviewpager.ohos:CoolViewPager:1.0.0'
    }
```


#### 使用说明：

第一步： 在布局文件中添加控件；
```xml
...
    <com.huanhailiuxin.coolviewpager.CoolViewPager
        ohos:id="$+id:vp"
        ohos:height="match_parent"
        ohos:width="match_parent"
        app:cvp_infiniteloop="true"
        app:cvp_autoscroll="true"/>
...

```
第二步：在对应的页面中找到该控件，设置适配器，滑动方向，是否循环滑动，自动播放等
```java
        @Override
            public void onStart(Intent intent) {
                super.onStart(intent);
                super.setUIContent(ResourceTable.Layout_ability_orientation);
                vp = (CoolViewPager) findComponentById(ResourceTable.Id_vp);
                vp.setScrollMode(CoolViewPager.ScrollMode.HORIZONTAL);
                vp.setInfiniteLoop(true);
                vp.setAdapter(adapter1);
                vp.setPageTransformer(new com.huanhailiuxin.coolviewpager.transformer.VerticalZoomInTransformer());
                ...
            }
```
自定义属性：我们可以通过xml或Java代码的方式设置CoolViewPager实例的属性

| attribute name | description |
|:---|:---|
| cvp_scrollmode | 滚动方向 |
| cvp_autoscroll | 是否开启自动滚动 |
| cvp_intervalinmillis | 自动滚动时间间隔 |
| cvp_autoscrolldirection | 自动滚动方向 |
| cvp_infiniteloop | 是否循环滚动 |
| cvp_scrollduration | 自动滚动耗时 |

#### 版本迭代

- v1.0.0

- CoolViewPager ohos 化组件对标的功能具体如下：

1. 自定义是否循环滚动；

2. 自定义自动滚动间隔时间，是否自动滚动；

3. 滚动方向。

- 未实现的功能如下：

1. 扩散效果，以及扩散颜色

2. 3D翻转动画

3. 无法屏蔽原有页面切换动画效果

####  版权与协议信息
```
    Copyright 2018 HuanHaiLiuXin
  
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
     http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
```
