package com.huanhailiuxin.coolviewpager.adapter;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.PageSliderProvider;
import ohos.utils.PlainArray;

/**
 * 作者:幻海流心
 * GitHub:https://github.com/HuanHaiLiuXin
 * 邮箱:wall0920@163.com
 * 2018/1/24 9:20
 *
 * <p>
 * 重写getItemPosition,解决原生PagerAdapter实例调用{@link PageSliderProvider#notifyDataChanged()},ViewPager界面不刷新的问题
 * </p>
 */

public class PagerAdapterWrapper extends PageSliderProvider {
    private PageSliderProvider mAdapter;
    private PlainArray mViewArray = new PlainArray();

    public PagerAdapterWrapper(PageSliderProvider adapter) {
        this.mAdapter = adapter;
    }

    @Override
    public int getCount() {
        return mAdapter == null ? 0 : mAdapter.getCount();
    }


    @Override
    public boolean isPageMatchToObject(Component component, Object object) {
        return mAdapter.isPageMatchToObject(component, object);
    }

    /* @Override
     public boolean isViewFromObject(Component view, Object object) {
         return mAdapter.isViewFromObject(view,object);
     }*/
    @Override
    public Object createPageInContainer(ComponentContainer container, int position) {
        Object item = mAdapter.createPageInContainer(container, position);
        int childCount = container.getChildCount();
        for (int i = 0; i < childCount; i++) {
            Component child = container.getComponentAt(i);
            if (isPageMatchToObject(child, item)) {
                mViewArray.put(position, child);
                break;
            }
        }
        return item;
    }


    /*  @Override
      public Object instantiateItem(ComponentContainer container, int position) {
          return mAdapter.instantiateItem(container,position);
      }*/
    @Override
    public void destroyPageFromContainer(ComponentContainer container, int position, Object object) {
        mAdapter.destroyPageFromContainer(container, position, object);
        mViewArray.remove(position);
    }


    /**
     * 解决原生PagerAdapter实例调用{@link PageSliderProvider#notifyDataChanged()},ViewPager界面不刷新的问题
     * 详见:http://www.07net01.com/program/642011.html
     *
     * @param object
     * @return
     */

    @Override
    public int getPageIndex(Object object) {
        int mChildCount = getCount();
        if (mChildCount > 0) {
            return POSITION_INVALID;
        }
        return super.getPageIndex(object);
    }

    public Component getViewAtRealPosition(int position) {
        return (Component) mViewArray.get(position).get();
    }
}
