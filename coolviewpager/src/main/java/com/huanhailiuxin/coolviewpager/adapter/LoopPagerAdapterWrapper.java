package com.huanhailiuxin.coolviewpager.adapter;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.PageSliderProvider;
import ohos.utils.PlainArray;

import static java.lang.Integer.MAX_VALUE;

/**
 * 作者:幻海流心
 * GitHub:https://github.com/HuanHaiLiuXin
 * 邮箱:wall0920@163.com
 * 2018/1/4 14:28
 */

public class LoopPagerAdapterWrapper extends PageSliderProvider {
    private PageSliderProvider mAdapter;
    private boolean mIsHandLoop;
    private PlainArray mViewArray = new PlainArray();

    public LoopPagerAdapterWrapper(PageSliderProvider adapter,boolean isHandLoop) {
        this.mAdapter = adapter;
        this.mIsHandLoop = isHandLoop;
    }

    @Override
    public int getCount() {
//        return mAdapter.getCount() + 2;
        return mIsHandLoop ? MAX_VALUE : getRealCount();
    }


    public int getRealCount() {
        return mAdapter.getCount();
    }

    public int toRealPosition(int position) {
//        int realCount = getRealCount();
//        if (realCount == 0) {
//            return 0;
//        } else {
//            int realPosition = (position - 1) % realCount;
//            if (realPosition < 0) {
//                realPosition += realCount;
//            }
//            return realPosition;
//        }
        return position % getRealCount();
    }

    public int toInnerPosition(int realPosition) {
        int position = realPosition + 1;
        return position;
    }

    public int getRealFirstPosition() {
        return 1;
    }

    public int getRealLastPosition() {
        return getRealFirstPosition() + getRealCount() - 1;
    }

    public PageSliderProvider getRealAdapter() {
        return mAdapter;
    }

    @Override
    public boolean isPageMatchToObject(Component component, Object object) {
        return mAdapter.isPageMatchToObject(component, object);
    }

    @Override
    public Object createPageInContainer(ComponentContainer container, int position) {
        int realPosition = toRealPosition(position);
        Object item = mAdapter.createPageInContainer(container, realPosition);
        int childCount = container.getChildCount();
        for (int i = 0; i < childCount; i++) {
            Component child = container.getComponentAt(i);
            if (isPageMatchToObject(child, item)) {
                mViewArray.put(realPosition, child);
                break;
            }
        }
        return item;
    }

    @Override
    public void destroyPageFromContainer(ComponentContainer container, int position, Object object) {
       int realPosition = toRealPosition(position);
        mAdapter.destroyPageFromContainer(container, realPosition, object);
        container.removeComponent((Component) object);
        mViewArray.remove(realPosition);
    }

    /**
     * 解决原生PagerAdapter实例调用{@link PageSliderProvider#notifyDataChanged()},ViewPager界面不刷新的问题
     * 详见:http://www.07net01.com/program/642011.html
     *
     * @param
     * @return
     */
    @Override
    public int getPageIndex(Object object) {
        int mChildCount = getCount();
        if (mChildCount > 0) {
            return POSITION_INVALID;
        }
        return super.getPageIndex(object);
    }

    public Object getPageFromVPChild(Component child) {
        int key = indexOfValue(child);
        if (key < 0) {
            return null;
        } else {
            return getViewAtRealPosition(key);
        }
    }

    public int indexOfValue(Object value) {
        return mViewArray.indexOfValue(value);
    }

    public Component getViewAtRealPosition(int position) {
        return (Component) mViewArray.get(position).get();
    }
}