package com.huanhailiuxin.coolviewpager;

import com.huanhailiuxin.coolviewpager.adapter.LoopPagerAdapterWrapper;
import com.huanhailiuxin.coolviewpager.adapter.PagerAdapterWrapper;
import ohos.agp.components.*;
import ohos.app.Context;
import ohos.app.dispatcher.TaskDispatcher;
import ohos.app.dispatcher.task.TaskPriority;

import static java.lang.Integer.MAX_VALUE;

public class CoolViewPager extends PageSlider implements ICoolViewPagerFeature, PageSlider.PageChangedListener {

    private Context mContext;

    PageSliderProvider mAdapter;
    int mCurItem;   // Index of currently displayed page.
    private ScrollMode mScrollMode = ScrollMode.HORIZONTAL;
    private boolean mAutoScroll = false;
    private int mIntervalInMillis = 2000;
    private AutoScrollDirection mAutoScrollDirection = AutoScrollDirection.FORWARD;
    private TimerHandler timer;
    private boolean mInfiniteLoop = false;
    private int mScrollDuration = 0;
    private PageTransformer mPageTransformer;

    private TimerHandler.TimerHandlerListener mTimerHandlerListener = new TimerHandler.TimerHandlerListener() {
        @Override
        public void callBack() {
            autoScrollNextPage();
        }
    };
    private PageSliderProvider realAdapter;


    public CoolViewPager(Context context) {
        super(context);
        this.mContext = context;
        init();
    }

    public CoolViewPager(Context context, AttrSet attrSet) {
        super(context, attrSet);
        this.mContext = context;
        init();
        initAttr(context, attrSet);  // 加载自定义属性
    }

    public CoolViewPager(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        this.mContext = context;
        init();
        initAttr(context, attrSet);  // 加载自定义属性
    }

    private void init() {
        addPageChangedListener(this);
    }

    private void initAttr(Context context, AttrSet attrs) {
        if (attrs != null) {
            if (attrs.getAttr("cvp_scrollmode").isPresent()) {
                if (attrs.getAttr("cvp_scrollmode").get().getStringValue().equals("vertical")) {
                    mScrollMode = ScrollMode.getScrollMode(ScrollMode.VERTICAL.id);
                } else if (attrs.getAttr("cvp_scrollmode").get().getStringValue().equals("horizontal")) {
                    mScrollMode = ScrollMode.getScrollMode(ScrollMode.HORIZONTAL.id);
                }
            }
            if (attrs.getAttr("cvp_autoscroll").isPresent()) {
                mAutoScroll = attrs.getAttr("cvp_autoscroll").get().getBoolValue();
            }
            if (attrs.getAttr("cvp_intervalinmillis").isPresent()) {
                mIntervalInMillis = attrs.getAttr("cvp_intervalinmillis").get().getIntegerValue();
            }
            if (attrs.getAttr("cvp_autoscrolldirection").isPresent()) {
                mAutoScrollDirection = AutoScrollDirection.getAutoScrollDirection(attrs.getAttr("cvp_autoscrolldirection").get().getIntegerValue());
            }
            if (attrs.getAttr("cvp_infiniteloop").isPresent()) {
                mInfiniteLoop = attrs.getAttr("cvp_infiniteloop").get().getBoolValue();
            }
            if (attrs.getAttr("cvp_scrollduration").isPresent()) {
                mScrollDuration = attrs.getAttr("cvp_scrollduration").get().getIntegerValue();
            }
        }
        setScrollMode(mScrollMode);
        setAutoScroll(mAutoScroll, mIntervalInMillis);
        setAutoScrollDirection(mAutoScrollDirection);
    }

    private void startTimer() {

        if (timer == null || !timer.isStopped) {
            return;
        }
        timer.listener = mTimerHandlerListener;
        timer.removeAllEvent();
        timer.tick();
        timer.isStopped = false;
    }

    private void stopTimer() {
        if (timer == null || timer.isStopped) {
            return;
        }
        timer.removeAllEvent();
        timer.listener = null;
        timer.isStopped = true;
    }

    /**
     * 获取在自动滚动开启情况下,当前实例要展示的下一页的索引值
     *
     * @return
     */
    private int getAutoScrollNextItem() {
        int nextItem = 0;
        mCurItem = getCurrentPage();
        if (mAutoScrollDirection == AutoScrollDirection.FORWARD) {
            nextItem = mCurItem + 1;
        } else {
            nextItem = mCurItem - 1;
        }
        return nextItem;
    }

    /**
     * 如果设置了Adapter,且Adapter中项数大于1,且开启了自动滚动,开启循环滚动
     */
    private void checkAndStartTimer() {
        if (this.mAdapter != null && this.mAdapter.getCount() > 1 && this.mAutoScroll) {
            startTimer();
        }
    }

    private void onPageScrolled(int position, float offset, int offsetPixels) {
        onPageTransformer(position, offset, offsetPixels);
    }

    private void onPageTransformer(int position, float positionOffset, int offsetPixels) {
        if (offsetPixels > 0) {
            scrollNextPage(position, positionOffset);
        } else {
            scrollPrePage(position, positionOffset);
        }
    }

    // 上一页
    private void scrollPrePage(int position, float positionOffset) {
        if (position - 1 >= 0) {
            Component in = ((PagerAdapterWrapper) mAdapter).getViewAtRealPosition(position - 1);
            mPageTransformer.transformPage(in, -1 + positionOffset);
        }
        Component out = ((PagerAdapterWrapper) mAdapter).getViewAtRealPosition(position);
        mPageTransformer.transformPage(out, positionOffset);
    }

    // 下一页
    private void scrollNextPage(int position, float positionOffset) {
        Component in = ((PagerAdapterWrapper) mAdapter).getViewAtRealPosition(position + 1);
        Component out = ((PagerAdapterWrapper) mAdapter).getViewAtRealPosition(position);
        mPageTransformer.transformPage(in, 1 - positionOffset);
        mPageTransformer.transformPage(out, 0 - positionOffset);
    }

    // ============================ public ========================================================

    public void setAdapter(PageSliderProvider adapter) {
        this.realAdapter = adapter;
        stopTimer();
        if (mInfiniteLoop && adapter.getCount() > 1) {
            LoopPagerAdapterWrapper adapterWrapper = new LoopPagerAdapterWrapper(adapter,mInfiniteLoop);
            this.mAdapter = adapterWrapper;
            mCurItem = adapterWrapper.getRealCount() * (MAX_VALUE / 5);
            setProvider(adapterWrapper);
            setCurrentPage(mCurItem,false);
        } else {
            mInfiniteLoop = false;
            mAdapter = new PagerAdapterWrapper(adapter);
            setProvider(mAdapter);
        }
        checkAndStartTimer();
    }

    public void notifyDataChanged() {
        setAdapter(realAdapter);
    }

    @Override
    public void setScrollMode(ScrollMode scrollMode) {
        setOrientation(scrollMode.id);
    }

    @Override
    public void setAutoScroll(boolean autoScroll, int... intervalInMillis) {
        this.mAutoScroll = autoScroll;
        if (intervalInMillis.length > 0 && intervalInMillis[0] > 0) {
            this.mIntervalInMillis = intervalInMillis[0];
        }

        if (!this.mAutoScroll) {
            this.stopTimer();
            this.timer = null;
        } else {
            this.timer = new TimerHandler(this, this.mTimerHandlerListener, (long) this.mIntervalInMillis);
            this.checkAndStartTimer();
        }
    }

    @Override
    public void setAutoScrollDirection(AutoScrollDirection autoScrollDirection) {
        this.mAutoScrollDirection = autoScrollDirection;
    }

    @Override
    public void autoScrollNextPage() {
        if (this.mAutoScroll && this.mAdapter != null && this.mAdapter.getCount() > 1) {
            int nextItem = getAutoScrollNextItem();
            setCurrentPage(nextItem);
        }
    }

    @Override
    public void setInfiniteLoop(boolean infiniteLoop) {
        if (this.mInfiniteLoop != infiniteLoop) {
            this.mInfiniteLoop = infiniteLoop;
            if (this.mAdapter != null) {
                if (this.mInfiniteLoop && !(this.mAdapter instanceof LoopPagerAdapterWrapper)) {
                    //对于原生PagerAdapter实例,如果Item数量<=1,则不应该让其循环滚动
                    if (this.mAdapter.getCount() > 1) {
                        setAdapter(this.mAdapter);
                    } else {
                        this.mInfiniteLoop = false;
                    }
                } else if (!this.mInfiniteLoop && this.mAdapter instanceof LoopPagerAdapterWrapper) {
                    setAdapter(((LoopPagerAdapterWrapper) this.mAdapter).getRealAdapter());
                }
            }
        }
    }

    @Override
    public void setDrawEdgeEffect(boolean drawEdgeEffect) {

    }

    @Override
    public void setEdgeEffectColor(int color) {

    }

    @Override
    public void setScrollDuration(boolean ifSetScrollDuration, int... scrollDuration) {
        if (ifSetScrollDuration && scrollDuration != null && scrollDuration.length > 0 && scrollDuration[0] > 0) {
            this.mScrollDuration = scrollDuration[0];
            setPageSwitchTime(mScrollDuration);
        } else {
            this.mScrollDuration = 0;
        }
    }

    public void toggleAutoScrollDirection() {
        if (this.mAutoScrollDirection == CoolViewPager.AutoScrollDirection.FORWARD) {
            this.mAutoScrollDirection = CoolViewPager.AutoScrollDirection.BACKWARD;
        } else {
            this.mAutoScrollDirection = CoolViewPager.AutoScrollDirection.FORWARD;
        }

    }

    public void setPageTransformer(PageTransformer pageTransformer) {
        this.mPageTransformer = pageTransformer;
    }

    @Override
    public void onPageSliding(int position, float offset, int offsetPixels) {
        if (mPageTransformer != null) {
            onPageScrolled(position, offset, offsetPixels);
        }
    }

    @Override
    public void onPageSlideStateChanged(int i) {

    }

    @Override
    public void onPageChosen(int i) {

    }

    /**
     * 获取当前CoolViewPager实例选中的页面,其在用户提供的原始Adapter中的实际位置,
     * 在例如需要变更PageIndicator的情况下会用到
     *
     * @return
     */
    public int getRealCurrentItem(){
        int realCurrentItem = -1;
        if (mAdapter instanceof LoopPagerAdapterWrapper) {
            realCurrentItem = ((LoopPagerAdapterWrapper)mAdapter).toRealPosition(getCurrentPage());
        }else {
            realCurrentItem =  getCurrentPage();
        }
        return realCurrentItem;
    }

    public int getIntervalInMillis() {
        return this.mIntervalInMillis;
    }

    public AutoScrollDirection getAutoScrollDirection() {
        return this.mAutoScrollDirection;
    }

    public int getScrollDuration() {
        return this.mScrollDuration;
    }

    /**
     * 滑动方向枚举类
     */
    public enum ScrollMode {
        HORIZONTAL(0), VERTICAL(1);
        int id;

        ScrollMode(int id) {
            this.id = id;
        }

        static ScrollMode getScrollMode(int id) {
            for (ScrollMode scrollMode : values()) {
                if (scrollMode.id == id)
                    return scrollMode;
            }
            throw new IllegalArgumentException();
        }
    }

    /**
     * 自动滚动方向枚举类
     */
    public enum AutoScrollDirection {
        FORWARD(0), BACKWARD(1);
        int id;

        AutoScrollDirection(int id) {
            this.id = id;
        }

        static AutoScrollDirection getAutoScrollDirection(int id) {
            for (AutoScrollDirection autoScrollDirection : values()) {
                if (autoScrollDirection.id == id) {
                    return autoScrollDirection;
                }
            }
            throw new IllegalArgumentException();
        }
    }

    /**
     * 动画接口
     */
    public interface PageTransformer {
        void transformPage(Component page, float position);
    }
}
