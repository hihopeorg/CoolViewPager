package com.huanhailiuxin.coolviewpager.transformer;

import com.huanhailiuxin.coolviewpager.CoolViewPager;
import ohos.agp.components.Component;

public class ZoomInTransformer implements CoolViewPager.PageTransformer {

    @Override
    public void transformPage(Component page, float position) {
        if(position <= -1 || position >= 1){
            page.setTranslationX(0F);
            page.setScaleX(1.0F);
            page.setScaleY(1.0F);
            page.setAlpha(1.0F);
        }else{
            page.setTranslationX(page.getWidth() * -position);
            final float scale = position < 0 ? position + 1F : Math.abs(1F - position);
            page.setScaleX(scale);
            page.setScaleY(scale);
            page.setPivotX(page.getWidth() * 0.5F);
            page.setPivotY(page.getHeight() * 0.5F);
            page.setAlpha(position < -1F || position > 1F ? 0F : 1F - (scale - 1F));
        }
    }

}
