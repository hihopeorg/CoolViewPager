package com.huanhailiuxin.coolviewpager.transformer;

import com.huanhailiuxin.coolviewpager.CoolViewPager;
import ohos.agp.components.Component;

/**
 * DefaultVerticalTransformer是CoolViewPager在垂直方向滚动的情况下默认的PageTransformer
 * <p>
 * 作者:幻海流心
 * GitHub:https://github.com/HuanHaiLiuXin
 * 邮箱:wall0920@163.com
 * 2018/1/16 11:31
 */

public class DefaultVerticalTransformer implements CoolViewPager.PageTransformer {

    @Override
    public void transformPage(Component page, float position) {
        page.setTranslationX(page.getWidth() * -position);
        page.setTranslationY(page.getHeight() * position);
    }
}
