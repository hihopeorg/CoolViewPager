package com.huanhailiuxin.demo.coolviewpager;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class AbilityEdgeEffectColor extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_edge_effect_color);
    }
}
