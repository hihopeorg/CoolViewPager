package com.huanhailiuxin.demo.coolviewpager;

import com.huanhailiuxin.coolviewpager.CoolViewPager;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;

import java.util.ArrayList;
import java.util.List;

public class EnableNotifyAbility extends BaseAbility implements Component.ClickedListener{
    private CoolViewPager vp;
    AbilityOrientation.MyAdapter adapter;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_enable_notify);
        findComponentById(ResourceTable.Id_buttonClick).setClickedListener(this);
        initViews();
    }

    private List<Component> items;

    private void initViews() {
        items = new ArrayList<>();
        items.add(createImageView(ResourceTable.Media_img3));
        items.add(createImageView(ResourceTable.Media_img5));
        items.add(createImageView(ResourceTable.Media_img6));
        items.add(createImageView(ResourceTable.Media_img7));
        adapter = new AbilityOrientation.MyAdapter(items);
        //
        vp = (CoolViewPager) findComponentById(ResourceTable.Id_vp);
        vp.setScrollMode(CoolViewPager.ScrollMode.HORIZONTAL);
        vp.setAdapter(adapter);
    }

    @Override
    public void onClick(Component component) {
        items.clear();
        items.add(createImageView(ResourceTable.Media_i1));
        items.add(createImageView(ResourceTable.Media_i2));
        items.add(createImageView(ResourceTable.Media_i5));
//        items.add(createImageView(ResourceTable.Media_i6));
        vp.notifyDataChanged();
    }

}
