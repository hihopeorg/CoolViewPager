package com.huanhailiuxin.demo.coolviewpager;

import com.huanhailiuxin.coolviewpager.CoolViewPager;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;

import java.util.ArrayList;
import java.util.List;

public class AbilityAutoScroll extends BaseAbility implements Component.ClickedListener {
    private CoolViewPager vp;
    AbilityOrientation.MyAdapter adapter;
    int index = 0;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_auto_scroll);
        findComponentById(ResourceTable.Id_buttonClick).setClickedListener(this);
        initViews();
    }

    private void initViews() {
        List<Component> items = new ArrayList<>();
        items.add(createImageView(ResourceTable.Media_img3));
        items.add(createImageView(ResourceTable.Media_img5));
        items.add(createImageView(ResourceTable.Media_img6));
        items.add(createImageView(ResourceTable.Media_img7));
        adapter = new AbilityOrientation.MyAdapter(items);
        //
        vp = (CoolViewPager) findComponentById(ResourceTable.Id_vp);
        vp.setScrollMode(CoolViewPager.ScrollMode.VERTICAL);
        vp.setAutoScroll(true,3000);
        vp.setScrollDuration(true,2000);
        vp.setAdapter(adapter);
    }

    @Override
    public void onClick(Component component) {
        if(index == 0){
            vp.toggleAutoScrollDirection();
            vp.setScrollDuration(true,1000);
            index = 1;
        }else{
            vp.toggleAutoScrollDirection();
            vp.setScrollDuration(true,500);
            index = 0;
        }
    }
}
