package com.huanhailiuxin.demo.coolviewpager;

import com.huanhailiuxin.coolviewpager.CoolViewPager;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;

import java.util.ArrayList;
import java.util.List;

public class AbilityPageTransformer extends BaseAbility implements Component.ClickedListener {
    private CoolViewPager vp;
    AbilityOrientation.MyAdapter adapter;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_page_transformer);
        findComponentById(ResourceTable.Id_buttonClick).setClickedListener(this);
        initViews();
    }

    private void initViews() {
        vp = (CoolViewPager) findComponentById(ResourceTable.Id_vp);
        initData();
    }

    private void initData() {
        List<Component> items = new ArrayList<>();
        items.add(createImageView(ResourceTable.Media_img3));
        items.add(createImageView(ResourceTable.Media_img5));
        items.add(createImageView(ResourceTable.Media_img6));
        items.add(createImageView(ResourceTable.Media_img7));

        adapter = new AbilityOrientation.MyAdapter(items);
        vp.setAdapter(adapter);
    }

    private int currIndex = -1;
    private CoolViewPager.PageTransformer[] verticals = new CoolViewPager.PageTransformer[]{
            new com.huanhailiuxin.coolviewpager.transformer.VerticalAccordionTransformer(),
            new com.huanhailiuxin.coolviewpager.transformer.VerticalRotateTransformer(),
            new com.huanhailiuxin.coolviewpager.transformer.VerticalDepthPageTransformer(),
            new com.huanhailiuxin.coolviewpager.transformer.VerticalRotateDownTransformer(),
            new com.huanhailiuxin.coolviewpager.transformer.VerticalZoomInTransformer()
    };

    @Override
    public void onClick(Component component) {
        initData();
        currIndex = (++currIndex)%verticals.length;
        vp.setPageTransformer(verticals[currIndex]);
    }
}
