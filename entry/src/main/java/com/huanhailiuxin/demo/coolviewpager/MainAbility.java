package com.huanhailiuxin.demo.coolviewpager;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;
import ohos.agp.window.dialog.ToastDialog;

public class MainAbility extends Ability implements Component.ClickedListener {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        findComponentById(ResourceTable.Id_changeOrientation).setClickedListener(this);
        findComponentById(ResourceTable.Id_changeEdgeEffectColor).setClickedListener(this);
        findComponentById(ResourceTable.Id_changeAutoScroll).setClickedListener(this);
        findComponentById(ResourceTable.Id_changePageTransformer).setClickedListener(this);
        findComponentById(ResourceTable.Id_enableNotifyDataSetChanged).setClickedListener(this);
        findComponentById(ResourceTable.Id_compareWithSupport).setClickedListener(this);

    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_changeOrientation:
                startAbility("com.huanhailiuxin.demo.coolviewpager.AbilityOrientation");
                break;
            case ResourceTable.Id_changeEdgeEffectColor:
//                startAbility("com.huanhailiuxin.demo.coolviewpager.AbilityEdgeEffectColor");
                ToastDialog toastDialog = new ToastDialog(this);
                toastDialog.setText("效果缺失");
                toastDialog.show();
                break;
            case ResourceTable.Id_changeAutoScroll:
                startAbility("com.huanhailiuxin.demo.coolviewpager.AbilityAutoScroll");
                break;
            case ResourceTable.Id_changePageTransformer:
                startAbility("com.huanhailiuxin.demo.coolviewpager.AbilityPageTransformer");
                break;
            case ResourceTable.Id_enableNotifyDataSetChanged:
                startAbility("com.huanhailiuxin.demo.coolviewpager.EnableNotifyAbility");
                break;
            case ResourceTable.Id_compareWithSupport:
                startAbility("com.huanhailiuxin.demo.coolviewpager.SupportViewPagerAbility");
                break;
        }
    }

    private void startAbility(String path) {
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withBundleName(getBundleName())
                .withAbilityName(path)
                .build();
        intent.setOperation(operation);
        startAbility(intent);
    }
}
