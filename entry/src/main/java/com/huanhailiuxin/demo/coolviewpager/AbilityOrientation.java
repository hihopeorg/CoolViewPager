package com.huanhailiuxin.demo.coolviewpager;

import com.huanhailiuxin.coolviewpager.CoolViewPager;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.PageSliderProvider;

import java.util.ArrayList;
import java.util.List;

public class AbilityOrientation extends BaseAbility implements Component.ClickedListener {

    public CoolViewPager vp;
    MyAdapter adapter1;
    MyAdapter adapter2;
    private int index = 0;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_orientation);
        vp = (CoolViewPager) findComponentById(ResourceTable.Id_vp);
        findComponentById(ResourceTable.Id_buttonClick).setClickedListener(this);
        initViews();
    }

    private void initViews() {
        List<Component> items1 = new ArrayList<>();
        items1.add(createImageView(ResourceTable.Media_i1));
        items1.add(createImageView(ResourceTable.Media_i2));
        items1.add(createImageView(ResourceTable.Media_i5));
        items1.add(createImageView(ResourceTable.Media_i6));

        List<Component> items2 = new ArrayList<>();
        items2.add(createImageView(ResourceTable.Media_img3));
        items2.add(createImageView(ResourceTable.Media_img5));
        items2.add(createImageView(ResourceTable.Media_img6));
        items2.add(createImageView(ResourceTable.Media_img7));

        adapter1 = new MyAdapter(items1);
        adapter2 = new MyAdapter(items2);
        //
//        vp.setCircularModeEnabled(true);
//        vp.setProvider(adapter1);
        vp.setScrollMode(CoolViewPager.ScrollMode.HORIZONTAL);
        vp.setInfiniteLoop(true);
        vp.setAdapter(adapter1);
    }

    @Override
    public void onClick(Component component) {
        if(index % 2 == 0){
            vp.setScrollMode(CoolViewPager.ScrollMode.VERTICAL);
            vp.setAdapter(adapter2);
            index = 1;
        }else{
            vp.setScrollMode(CoolViewPager.ScrollMode.HORIZONTAL);
            vp.setAdapter(adapter1);
            index = 0;
        }
    }

    static class MyAdapter extends PageSliderProvider {
        // 数据源，每个页面对应list中的一项
        private List<Component> views;

        public MyAdapter(List<Component> list) {
            this.views = list;
        }

        @Override
        public int getCount() {
            return views.size();
        }

        @Override
        public Object createPageInContainer(ComponentContainer componentContainer, int position) {
            if (views.get(position).getComponentParent() != null) {
                ((ComponentContainer) views.get(position).getComponentParent()).removeComponent(views.get(position));
            }
            componentContainer.addComponent(views.get(position));
            return views.get(position);
        }

        @Override
        public void destroyPageFromContainer(ComponentContainer componentContainer, int i, Object o) {
            componentContainer.removeComponent((Component) o);
        }

        @Override
        public boolean isPageMatchToObject(Component component, Object o) {
            //可添加具体处理逻辑
            return component == o;
        }
    }

}
