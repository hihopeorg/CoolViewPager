package com.huanhailiuxin.demo.coolviewpager;

import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.PageSlider;
import ohos.agp.utils.Color;

import java.util.ArrayList;
import java.util.List;

public class SupportViewPagerAbility extends BaseAbility implements Component.ClickedListener {
    private List<Component> items;
    private AbilityOrientation.MyAdapter adapter;
    private PageSlider vp;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_support_view_pager);
        findComponentById(ResourceTable.Id_buttonClick).setClickedListener(this);
        vp = (PageSlider) findComponentById(ResourceTable.Id_vp);
        initViews();
    }

    private void initViews() {
        items = new ArrayList<>();
        items.add(createImageView(ResourceTable.Media_img3));
        items.add(createImageView(ResourceTable.Media_img5));
        items.add(createImageView(ResourceTable.Media_img6));
        items.add(createImageView(ResourceTable.Media_img7));
        adapter = new AbilityOrientation.MyAdapter(items);

        vp.setProvider(adapter);
    }
    @Override
    public void onClick(Component component) {
        items.clear();
        items.add(createImageView(ResourceTable.Media_i1));
        items.add(createImageView(ResourceTable.Media_i2));
        items.add(createImageView(ResourceTable.Media_i5));
        items.add(createImageView(ResourceTable.Media_i6));
        vp.setProvider(adapter);
    }
}
