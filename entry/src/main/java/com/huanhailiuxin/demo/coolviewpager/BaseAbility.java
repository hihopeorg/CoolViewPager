package com.huanhailiuxin.demo.coolviewpager;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Image;

import java.util.HashMap;
import java.util.Map;

public class BaseAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
//        super.setUIContent(ResourceTable.Layout_ability_base);
    }

    private Map<String, Integer> imgs = new HashMap<String, Integer>();

    public Component createImageView(int imgResId) {
        Image imageView = new Image(this);
        if (imgs.get("" + imgResId) != null) {
            imageView.setPixelMap(imgs.get("" + imgResId));
        } else {
            imgs.put("" + imgResId, imgResId);
            imageView.setPixelMap(imgResId);
        }
        return imageView;
    }

}
