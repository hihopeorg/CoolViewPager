/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.huanhailiuxin.demo.coolviewpager;


import com.huanhailiuxin.coolviewpager.CoolViewPager;
import com.huanhailiuxin.coolviewpager.adapter.LoopPagerAdapterWrapper;
import junit.framework.TestCase;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.agp.components.Button;
import ohos.app.Context;
import org.junit.Assert;

public class UICoolViewPagerTest extends TestCase {
    private static IAbilityDelegator sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
    private Context mContext;

    public void setUp() throws Exception {
        super.setUp();
        mContext = sAbilityDelegator.getAppContext();
    }

    public void tearDown() throws Exception {
        EventHelper.clearAbilities();
        sleep(3);
    }


    public void testOrientationAbility() {
        Ability orientationAbility = EventHelper.startAbility(AbilityOrientation.class);
        sleep(1);
        CoolViewPager vp = (CoolViewPager) orientationAbility.findComponentById(ResourceTable.Id_vp);
        EventHelper.inputSwipe(orientationAbility,vp,1000,200,10,200,1000);
        sleep(1);
        Assert.assertEquals(1, vp.getRealCurrentItem());
        sleep(1);

        Button btn = (Button) orientationAbility.findComponentById(ResourceTable.Id_buttonClick);
        EventHelper.triggerClickEvent(orientationAbility, btn);
        sleep(1);
        EventHelper.inputSwipe(orientationAbility,vp,200,1000,200,10,1000);
        sleep(1);
        Assert.assertEquals(1, vp.getRealCurrentItem());
        sleep(1);
    }

    public void testAbilityAutoScroll() {
        Ability autoScrollAbility = EventHelper.startAbility(AbilityAutoScroll.class);
        sleep(1);
        CoolViewPager vp = (CoolViewPager) autoScrollAbility.findComponentById(ResourceTable.Id_vp);
        Button btn = (Button) autoScrollAbility.findComponentById(ResourceTable.Id_buttonClick);
        EventHelper.triggerClickEvent(autoScrollAbility, btn);
        sleep(4);
        int duration = vp.getScrollDuration();
        Assert.assertEquals(1000, duration);
        sleep(1);
    }

    public void testRecorderVideoShowTime() {
        Ability notifyAbility = EventHelper.startAbility(EnableNotifyAbility.class);
        sleep(1);
        CoolViewPager vp = (CoolViewPager) notifyAbility.findComponentById(ResourceTable.Id_vp);
        Button btn = (Button) notifyAbility.findComponentById(ResourceTable.Id_buttonClick);
        EventHelper.triggerClickEvent(notifyAbility, btn);
        int count = ((LoopPagerAdapterWrapper)vp.getProvider()).getRealAdapter().getCount();
        sleep(1);
        Assert.assertEquals(3, count);
        sleep(1);
    }


    private void sleep(int duration) {
        try {
            Thread.sleep(duration * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}